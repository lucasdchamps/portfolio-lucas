The source code to my [programming blog](http://lucasdchamps.herokuapp.com) written in django.

The app was set up following [this tutorial](https://www.djangorocks.com/tutorials/how-to-create-a-basic-blog-in-django/)