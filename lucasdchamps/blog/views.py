from blog.models import Blog
from django.shortcuts import render_to_response, get_object_or_404

def index(request):
    return render_to_response('index.html', {
        # 'categories': Category.objects.all(),
        'posts': reversed(Blog.objects.all()[:10])
    })

def view_post(request, slug):
    return render_to_response('view_post.html', {
        'post': get_object_or_404(Blog, slug=slug)
    })

def about(request):
    return render_to_response('about.html')

# def view_category(request, slug):
#     category = get_object_or_404(Category, slug=slug)
#     return render_to_response('view_category.html', {
#         'category': category,
#         'posts': Blog.objects.filter(category=category)[:5]
#     })
